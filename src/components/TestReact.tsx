import { useStore } from "@nanostores/react";
import { testNumber } from "../stores/testStore";

export default function () {
  const $testNumber = useStore(testNumber);

  const increment = (event: React.SyntheticEvent) => {
    testNumber.set($testNumber + 1);
  };

  return (
    <div>
      <span>{$testNumber}</span>{" "}
      <button className="btn btn-blue" onClick={increment}>
        +
      </button>
    </div>
  );
}
