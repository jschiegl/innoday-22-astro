import { defineConfig } from "astro/config";

// https://astro.build/config
import react from "@astrojs/react";

// https://astro.build/config
import vue from "@astrojs/vue";
import tailwind from "@astrojs/tailwind";

// https://astro.build/config
export default defineConfig({
  outDir: "public",
  publicDir: "static",
  base: "/innoday-22-astro",
  site: "https://jschiegl.gitlab.io",
  integrations: [react(), vue() /*, tailwind()*/],
});
